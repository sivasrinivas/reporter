// Define the `reporterApp` module
var reporterApp = angular.module('reporterApp', []);

// Define the `ReporterController` controller on the `reporterApp` module
reporterApp.controller('reporterController', function ReporterController($scope, $http, $window, $interval) {
    $scope.statusChecker = null;
    $scope.customer = null;
    $scope.environment = null;
    $scope.newFile = null;
    $scope.error = null;
    $scope.status = null;
    $scope.filelist = null;

    //Get all existing files on page load
    $http.get("./filelist").then(function (response) {
        $scope.filelist = response.data;
    });

    $scope.generateReport = function () {
        //Start showing report generation status
        $scope.startStatusChecker();
        $http.get("./report?customer=" + $scope.customer + "&environment=" + $scope.environment).then(function (response) {
            $scope.newFile = response.data;
            //Stop polling status
            $scope.stopStatusChecker();
        }, function (error) {
            $scope.error = error;
            ////Stop polling status - TODO not working, need to check
            //$scope.stopStatusChecker();
        });
    };

    $scope.deleteFile = function (filename) {
        if ($window.confirm("Are you sure you want to delete " + filename + "?")) {
            $http.get("./file?delete=" + filename).then(function (response) {
                $scope.filelist = response.data;
            });
        }
    };

    $scope.startStatusChecker = function () {
        $scope.statusChecker = $interval(function () {
            $http.get("./status").then(function (response) {
                $scope.status = response.data;
            });
        }, 1000);
    };
    $scope.stopStatusChecker = function () {
        //Cancel the statusChecker.
        if (angular.isDefined($scope.statusChecker)) {
            $interval.cancel($scope.statusChecker);
        }
        //Set status to null
        $scope.status = null;
    };
});