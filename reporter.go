package main

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

type Customer struct {
	Name, RDS, UserName, Password, UAT, PROD string
}

type ReportFile struct {
	Name string `json:"name"`
	Path string `json:"path"`
}

var Customers = make(map[string]Customer, 5)
var status string

func checkError(e error) {
	if e != nil {
		log.Fatal(e)
	}
}

func parseValue(valueStr string) string {
	if len(strings.TrimSpace(valueStr)) == 0 {
		return "null"
	}
	value, err := strconv.ParseFloat(valueStr, 32)
	checkError(err)
	return fmt.Sprintf("%.2f", value)
}

func getFileList() []ReportFile {
	reportFileList := make([]ReportFile, 0)
	curDir, err := filepath.Abs(filepath.Dir("."))
	checkError(err)
	filepath.Walk("files/", func(path string, info os.FileInfo, err error) error {
		fName := info.Name()
		if strings.HasSuffix(fName, ".csv") {
			fullPath := fmt.Sprintf("%s/files/%s", curDir, fName)
			reportFile := ReportFile{fName, fullPath}
			reportFileList = append(reportFileList, reportFile)
		}
		return nil
	})
	return reportFileList
}

func reportHandler(w http.ResponseWriter, r *http.Request) {
	//Get customer and environment params
	cust := strings.ToUpper(r.URL.Query().Get("customer"))
	env := strings.ToUpper(r.URL.Query().Get("environment"))

	//Return if customer or environment is null
	if len(cust) < 1 || len(env) < 1 {
		return
	}
	//Return if environment is other than UAT, PROD
	if strings.Compare("UAT", env) != 0 && strings.Compare("PROD", env) != 0 {
		//If env is not either UAT or PROD, then return
		return
	}
	//Get customer struct (with properties)
	customer := Customers[cust]
	dbName := customer.UAT
	if strings.Compare("PROD", env) == 0 {
		dbName = customer.PROD
	}
	fmt.Println("Customer", customer)

	//Construct data source and open a DB connection
	dataSource := customer.UserName + ":" + customer.Password + "@tcp(" + customer.RDS + ":3306)/" + dbName + "?charset=utf8"
	dbCon, err := sql.Open("mysql", dataSource)
	checkError(err)
	dbCon.SetMaxIdleConns(1)
	defer dbCon.Close()
	fmt.Println("Data source name:", dataSource)
	log.Println("Initialized DB")

	columns, err := dbCon.Query("desc AccountProduct;")
	checkError(err)
	defer columns.Close()

	//Variables used to scan results
	var (
		buffer                                                                           bytes.Buffer
		field                                                                            string
		notRequired                                                                      interface{}
		nullCount, nonNullCount                                                          int
		min, perc10, perc20, perc30, perc40, perc50, perc60, perc70, perc80, perc90, max sql.NullString
	)

	buffer.WriteString("Field, Min, Perc10, Perc20, Perc30, Perc40, Perc50, Perc60, Perc70, Perc80, Perc90, Max, NonNullCount, NullCount\n")
	for columns.Next() {
		err = columns.Scan(&field, &notRequired, &notRequired, &notRequired, &notRequired, &notRequired)
		checkError(err)
		if strings.HasSuffix(field, "_akt") {
			//Set status so that front-end polls and update page
			status = fmt.Sprintf("Calculating %s metric distribution.", field)
			//set group_concat_max_len before executing query
			_, err = dbCon.Exec("set group_concat_max_len=10485760;")
			checkError(err)

			query := fmt.Sprintf("SELECT count(*) as NonNullCount, MIN(%s) AS Min, "+
				"SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(%[1]s ORDER BY %[1]s SEPARATOR ','), ',', 10/100*COUNT(*)+1), ',', -1) AS Perc10, "+
				"SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(%[1]s ORDER BY %[1]s SEPARATOR ','), ',', 20/100*COUNT(*)+1), ',', -1) AS Perc20, "+
				"SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(%[1]s ORDER BY %[1]s SEPARATOR ','), ',', 30/100*COUNT(*)+1), ',', -1) AS Perc30, "+
				"SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(%[1]s ORDER BY %[1]s SEPARATOR ','), ',', 40/100*COUNT(*)+1), ',', -1) AS Perc40, "+
				"SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(%[1]s ORDER BY %[1]s SEPARATOR ','), ',', 50/100*COUNT(*)+1), ',', -1) AS Perc50, "+
				"SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(%[1]s ORDER BY %[1]s SEPARATOR ','), ',', 60/100*COUNT(*)+1), ',', -1) AS Perc60, "+
				"SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(%[1]s ORDER BY %[1]s SEPARATOR ','), ',', 70/100*COUNT(*)+1), ',', -1) AS Perc70, "+
				"SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(%[1]s ORDER BY %[1]s SEPARATOR ','), ',', 80/100*COUNT(*)+1), ',', -1) AS Perc80, "+
				"SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(%[1]s ORDER BY %[1]s SEPARATOR ','), ',', 90/100*COUNT(*)+1), ',', -1) AS Perc90, "+
				"MAX(%[1]s) AS Max FROM AccountProduct WHERE %[1]s is not null;", field)
			err = dbCon.QueryRow(query).Scan(&nonNullCount, &min, &perc10, &perc20, &perc30, &perc40, &perc50, &perc60, &perc70, &perc80, &perc90, &max)
			checkError(err)
			query = "Select count(*) as NullCount from AccountProduct where " + field + " is null;"
			err = dbCon.QueryRow(query).Scan(&nullCount)
			checkError(err)

			buffer.WriteString(fmt.Sprintf("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d,%d\n",
				field, parseValue(min.String), parseValue(perc10.String), parseValue(perc20.String), parseValue(perc30.String), parseValue(perc40.String),
				parseValue(perc50.String), parseValue(perc60.String), parseValue(perc70.String), parseValue(perc80.String), parseValue(perc90.String),
				parseValue(max.String), nonNullCount, nullCount))
		}
	}
	columns.Close()
	//Reset status variable
	status = ""

	//write to a file
	fileName := customer.Name + "_" + dbName + "_metric_report_" + time.Now().Local().Format("2006-01-02-15-04-05") + ".csv"
	file, err := os.Create("files/" + fileName)
	checkError(err)
	_, err = file.Write(buffer.Bytes())
	checkError(err)
	file.Close()
	log.Println("Successfull completed generating report", fileName)

	curDir, err := filepath.Abs(filepath.Dir("."))
	checkError(err)
	newFile := ReportFile{fileName, curDir + "/files/" + fileName}
	jBytes, err := json.MarshalIndent(newFile, "", " ")
	checkError(err)
	w.Write(jBytes)
}

func fileListHandler(w http.ResponseWriter, r *http.Request) {
	var buf bytes.Buffer
	jBytes, err := json.MarshalIndent(getFileList(), "", " ")
	checkError(err)
	buf.Write(jBytes)
	w.Write(buf.Bytes())
}

func fileHandler(w http.ResponseWriter, r *http.Request) {
	downloadFName := r.URL.Query().Get("download")
	deleteFName := r.URL.Query().Get("delete")
	if len(downloadFName) > 0 {
		log.Println("Serving: " + downloadFName)
		http.ServeFile(w, r, "files/"+downloadFName)
	}
	if len(deleteFName) > 0 {
		log.Println("Deleting: " + deleteFName)
		os.Remove("files/" + deleteFName)
	}
	var buf bytes.Buffer
	jBytes, err := json.MarshalIndent(getFileList(), "", " ")
	checkError(err)
	buf.Write(jBytes)
	w.Write(buf.Bytes())
}

func statusHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("Sending status:", status)
	w.Write([]byte(status))
}

func init() {
	//Set all customer details
	Customers["BMSUS"] = Customer{"BMSUS", "bmsusrds.aktana.com", "bmsadmin", "cp0b00I50s8X4Dj4W3F00780a", "bmsusuat", "bmsusprod"}
	Customers["SUNOVIONUS"] = Customer{"SUNOVIONUS", "sunovionusrds.aktana.com", "sunovionadmin", "Lnn6PDhDimxkKXbn4XUGbgaMAJaPCR", "sunovionusuat", "sunovionusprod"}
	Customers["LILLYJA"] = Customer{"LILLYJA", "lillyjards.aktana.com", "lillyjaadmin", "lIh9ySAgbit49z8008F2t840w2twOG", "lillyjauat", "lillyjaprod"}
	Customers["GSKJA"] = Customer{"GSKJA", "gskjards.aktana.com", "gskadmin", "G56s5rt665ye8n95Q895Pk7pD7JtJb", "gskjauat", "gskjaprod"}
	Customers["MSDUK"] = Customer{"MSDUK", "msdukrds.aktana.com", "msdadmin", "LWyyDLqLqO5yuWbdsuXMorj6D", "msdukuat", "msdukprod"}
	log.Println("Initialized customers")
}

func main() {
	log.Println("Starting reporter application...")
	router := mux.NewRouter()
	router.HandleFunc("/status", statusHandler)
	router.HandleFunc("/report", reportHandler)
	router.HandleFunc("/filelist", fileListHandler)
	router.HandleFunc("/file", fileHandler)
	router.PathPrefix("/").Handler(http.FileServer(http.Dir("./static/")))
	http.Handle("/", router)
	http.ListenAndServe(":9999/", nil)
}
